import java.util.Arrays;
import java.util.List;

public class ListUtil {
	private static void printList(List<String> list){
//		System.out.print
		
	}
	
	/**
	 * Find the largest elemnent a List of Strings,
	 * using the String compareTo method.
	 * @return the lexically largest element in the List
	 */
	private static String max( List<String> list) {
		
	} 
	
	/** Test the max method. */
	public static void main(String[] args) {
		List<String> list;
		if(args.length > 0) list = Arrays.asList( args);
		else list = Arrays.asList("bird","zebra","cat","pig");
		
		System.out.println("List contains: ");
		printList (list);
		
		String max = max(list);
		System.out.println("Lexically greatest element is "+max);
	}
}
