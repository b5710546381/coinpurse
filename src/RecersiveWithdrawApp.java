import java.util.ArrayList;
import java.util.List;

import coinpurse.*;
import coinpurse.strategy.*;


public class RecersiveWithdrawApp {

	public static void main(String[] args) {
		RecursiveWithdraw r = new RecursiveWithdraw();
		List<Valuable> l = new ArrayList<Valuable>();
		l.add(new Coin(2));
		l.add(new Coin(2));
//		l.add(new Coin(1));
		l.add(new Coin(2));
		l.add(new Coin(5));
		List<Valuable> result = r.withdraw(6,l);
		if(result == null) System.out.println("Can't withdraw.");
		else  {
			for(Object v: result.toArray()) {
				System.out.println(v.toString());
			}
		}
	}

}
