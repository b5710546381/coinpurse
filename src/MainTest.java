import coinpurse.ConsoleDialog;
import coinpurse.Purse;
import coinpurse.strategy.GreedyWithdraw;
import coinpurse.strategy.RecursiveWithdraw;


public class MainTest {
	private static int CAPACITY = 4;
    public static void main( String[] args ) {
//TODO follow the steps in the sequence diagram
        // 1. create a Purse
    	Purse purse = new Purse(CAPACITY);
    	purse.setWithdrawStrategy(new RecursiveWithdraw());
        // 2. create a ConsoleDialog with a reference to the Purse object
    	ConsoleDialog console = new ConsoleDialog(purse);
        // 3. run() the ConsoleDialog
    	console.run();
    }
}
