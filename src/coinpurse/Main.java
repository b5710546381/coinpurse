package coinpurse;

import coinpurse.strategy.RecursiveWithdraw;
 
/**
 * A main class to create objects and connect objects together.
 * The user interface needs a reference to coin purse.
 */
public class Main {

    /**
     * @param args not used
     */
	private static int CAPACITY = 4;
    public static void main( String[] args ) {
//TODO follow the steps in the sequence diagram
        // 1. create a Purse
    	Purse purse = new Purse(CAPACITY);
    	
    	purse.addObserver(new BalanceObserver());
    	purse.addObserver(new StatusObserver(CAPACITY));
    	purse.setWithdrawStrategy(new RecursiveWithdraw());
        // 2. create a ConsoleDialog with a reference to the Purse object
    	ConsoleDialog console = new ConsoleDialog(purse);
        // 3. run() the ConsoleDialog
    	console.run();
    }
}
