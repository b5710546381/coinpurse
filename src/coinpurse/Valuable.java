package coinpurse;
/**
 * A specification for any kinds of money (Coin, Coupon, or BankNote)
 * @author Pakpon Jetapai
 * @version 2015.01.27
 */
public interface Valuable extends Comparable<Valuable> {

	/**
	 * Get the value of this money.
	 * @return value of this Valuable object.
	 */
	public double getValue();
	
	/**
	 * Show value of this money with Baht.
	 * return a String text of that object.
	 */
	public String toString();
    /**
     * check if this object has the same value as the other one.
     * @param obj is a Object to compare to this.
     */
	public boolean equals(Object obj);
}
