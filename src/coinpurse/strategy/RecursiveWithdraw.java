package coinpurse.strategy;
import java.util.ArrayList;
import java.util.List;

import coinpurse.*;
public class RecursiveWithdraw implements WithdrawStrategy {
	@Override
	public List<Valuable> withdraw(double amount, List<Valuable> valuables ){
		return withdrawFrom(amount, valuables, valuables.size()-1);
	}
	/**HELPER METHOD*/
	public List<Valuable> withdrawFrom (double remainingAmount, List<Valuable> valuables, int lastIndex){
		if(!(lastIndex < 0)){
			List<Valuable> tempList = new ArrayList<Valuable>();
			if(remainingAmount - (double) valuables.get(lastIndex).getValue() < 0) {
				return null;
			}
			double tempAmount = remainingAmount;
			tempAmount -= (double) valuables.get(lastIndex).getValue();
			if(tempAmount == 0){
				tempList.add(valuables.get(lastIndex));
				return tempList;
			}
			if( withdrawFrom (tempAmount, valuables, lastIndex-1) == null ) {
				List<Valuable> notPick = withdrawFrom(remainingAmount, valuables, lastIndex-1);
				tempList.addAll(notPick);
				return tempList;
			}
			remainingAmount = tempAmount;
			tempList.add(valuables.get(lastIndex));
			List<Valuable> pickAndRec = withdrawFrom(remainingAmount, valuables, lastIndex-1);
			tempList.addAll(pickAndRec);
			return tempList;
		}
		return null;
	}
}

