package coinpurse.strategy;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import coinpurse.Valuable;
import coinpurse.ValueComparator;

/**
 * A strategy for withdrawing by taking the largest value first.
 * @author Pakpon Jetapai
 */
public class GreedyWithdraw implements WithdrawStrategy {
	@Override
	public List<Valuable> withdraw(double amount, List<Valuable> valuables) {
		//TODO don't allow to withdraw amount < 0
		//if ( ??? ) return ???;
		/*
		 * One solution is to start from the most valuable coin
		 * in the purse and take any coin that maybe used for
		 * withdraw.
		 * Since you don't know if withdraw is going to succeed, 
		 * don't actually withdraw the coins from the purse yet.
		 * Instead, create a temporary list.
		 * Each time you see a coin that you want to withdraw,
		 * add it to the temporary list and deduct the value
		 * from amount. (This is called a "Greedy Algorithm".)
		 * Or, if you don't like changing the amount parameter,
		 * use a local total to keep track of amount withdrawn so far.
		 * 
		 * If amount is reduced to zero (or tempTotal == amount), 
		 * then you are done.
		 * Now you can withdraw the coins from the purse.
		 * NOTE: Don't use list.removeAll(templist) for this
		 * because removeAll removes *all* coins from list that
		 * are equal (using Coin.equals) to something in templist.
		 * Instead, use a loop over templist
		 * and remove coins one-by-one.
		 */
		//TODO code for withdraw
		// Did we get the full amount?
		if ( amount < 0 ) {
			// failed. Since you haven't actually remove
			// any coins from Purse yet, there is nothing
			// to put back.
			return null;
		}

		// Success.
		// Since this method returns an array of Coin,
		// create an array of the correct size and copy
		// the Coins to withdraw into the array.
		Collections.sort(valuables);
		ArrayList<Valuable> temp = new ArrayList<Valuable>();
		for (int i = 0;i < valuables.size();i++) {
			if ( (double) valuables.get(i).getValue() <= amount ) {
				temp.add((Valuable) valuables.get(i));
				amount -= (double) valuables.get(i).getValue();
			}
		}
		if (temp.size() >0 &&amount == 0) {
			for(int i = 0;i<temp.size();i++){
				valuables.remove((Valuable) temp.get(i));
			}
			List<Valuable> withdrawList = new ArrayList<Valuable>();
			for(int i = 0;i<temp.size();i++){
				withdrawList.add(temp.get(i));
			}
			return withdrawList;
		}
		return null;
	}

}
