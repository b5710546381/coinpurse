package coinpurse.strategy;
import java.util.List;

import coinpurse.Purse;
import coinpurse.Valuable;

/**
 * Interface for performing withdraw.
 * @author Pakpon Jetapai
 */
public interface WithdrawStrategy {
	/**Withdraw a list of Valuable by using specifying strategy.*/
	public List<Valuable> withdraw(double amount, List<Valuable> valuables);
}
