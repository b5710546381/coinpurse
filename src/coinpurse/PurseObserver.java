package coinpurse;

import java.util.Observable;

public class PurseObserver implements java.util.Observer {
	public PurseObserver() {
		// no reference to purse necessary
	}
	/** update receives notification from the purse */
	public void update(Observable subject, Object info) {
		if(subject instanceof Purse ) {
			Purse purse = (Purse) subject;
			double balance = purse.getBalance();
			System.out.println("Balance is: "+ balance);
		}
		if(info != null) System.out.println( info );
	}
}
