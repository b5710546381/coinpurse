package coinpurse;
/**
 * A BankNote with a monetary value.
 * BankNote's value cannot be changed.
 * @author Pakpon Jetapai
 *
 */
public class BankNote extends AbstractValuable{
	/**serialNumber of the BankNote*/
	private int serialNumber;
	/**value for the next serialNumber*/
	private static int nextSerialNumber = 1000000;
	/**
	 * Initialize BankNote object
	 * BankNote object contains value and its serialNumber.
	 * @param value to be put in the value of this BankNote.
	 */
	public BankNote(double value){
		super(value);
		serialNumber = getNextSerialNumber();
	}
	/**
	 * Show a String text of this BankNote.
	 * @return a String format such as "xxx-Baht Banknote [serialnum]".
	 */
	public String toString(){
		return String.format("%f-Baht BankNote [%d]", this.getValue(),serialNumber);
	}
	/**
	 * get serial Number and increment it by 1.
	 * @return nextSerialNumber and increment by 1.
	 */
	public int getNextSerialNumber(){
		return nextSerialNumber++;
	}
	/**
	 * get value of this.
	 * @return value of the BankNote.
	 */
	@Override
	public double getValue() {
		return value;
	}
	
}
