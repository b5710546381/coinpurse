package coinpurse;

import java.util.Observable;

import javax.swing.JFrame;
import javax.swing.JLabel;
/**
 * An observer that keeps displaying the balance in the Purse.
 * @author Pakpon Jetapai
 */
public class BalanceObserver implements java.util.Observer, Runnable {
	private JFrame balanceFrame;
	private JLabel balanceLabel;
	/**Initialize BalanceObserver and creates included components.*/
	public BalanceObserver() {
		this.initComponents();
	}
	private void initComponents() {
		balanceFrame = new JFrame("Purse Balance");
		balanceLabel = new JLabel();
		balanceFrame.add(balanceLabel);
	}
	/**Set BalanceObserver's frame visible and resizes it to fit the pane.*/
	public void run(){
		balanceFrame.setVisible(true);
		balanceFrame.pack();
	}
	/**Update receives notification from the purse by showing the current balance of the Purse.
	 * @param subject an observing object to be used for displaying balance.
	 * @param info is not used in this method.
	 */ 
	public void update(Observable subject, Object info) {
		if(subject instanceof Purse ) {
			Purse purse = (Purse) subject;
			double balance = purse.getBalance();
			balanceLabel.setText(String.valueOf(balance));
			this.run();
		}
		if(info != null) System.out.println( info );
	}
}
