package coinpurse;
/**
 *A superclass of Coin, Coupon, and BankNote. 
 * @author Pakpon Jetapai
 *
 */
public abstract class AbstractValuable implements Valuable {
	/**value of the this*/
	public double value;
	public AbstractValuable(double value){
		this.value = value;
	}
	/**
	 * check if this object has the same value as the other one.
	 * @param obj is a Object to compare to this.
	 * @return is true if a) obj is not null, b) obj is a Coin, c) obj has same value as this
	 */
	public boolean equals(Object obj){
		if(obj == null) return false;
		if(this.getClass() != obj.getClass()) return false;
		Valuable other = (Valuable) obj;
		if(this.value == other.getValue()) return true;
		return false;
	}
	/**
	 * Compare value between a and b
	 * @param a is a Valuable object that will compare with b
	 * @param b is a Valuable object that will compare with a
	 */
	@Override
	public int compareTo(Valuable a){
		return (int) Math.signum(this.value-a.getValue());
	}
}