package coinpurse;
/**
 * A coin with a monetary value.
 * You can't change the value of a coin.
 * @author Pakpon Jetapai
 */
public class Coin extends AbstractValuable implements Valuable {

    /** 
     * Constructor for a new coin. 
     * @param value is the value for the coin
     */
    public Coin( double value ) {
    	super(value);
    }
    
    /**
     * get value of this coin.
     * @return value of this coin that it has.
     */
    public double getValue(){
    	return super.value;
    }
    
    /**
     * print out amount of value in Baht
     * @return String of value with Baht at the end
     */
    public String toString(){
    	return String.format(this.getValue()+"-Baht coin");
    }
}
