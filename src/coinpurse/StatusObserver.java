package coinpurse;

import java.util.Observable;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
/**
 * An observer that keeps displaying the current amount of Valuable in the Purse.
 * @author Pakpon Jetapai
 */
public class StatusObserver implements Runnable, java.util.Observer {
	JFrame statusFrame;
	JPanel mainPanel;
	JLabel statusLabel;
	JProgressBar progressBar;
	/**
	 * Initialize StatusObserver as well as it's included components.
	 * @param capacity a number for controlling maximum progress bar.
	 */
	public StatusObserver(int capacity) {
		this.initComponents(capacity);
	}
	private void initComponents(int capacity) {
		statusFrame = new JFrame("Purse Status");
		mainPanel = new JPanel();
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
		statusLabel= new JLabel();
		progressBar = new JProgressBar(0, capacity);
		mainPanel.add(statusLabel);
		mainPanel.add(progressBar);
		statusFrame.add(mainPanel);
	}
	/** Set StatusObserver's frame visible and resizes it to fit the pane. */
	public void run() {
		statusFrame.setVisible(true);
		statusFrame.pack();
	}
	/**Update receives notification from the purse by showing the current balance of the Purse.
	 * @param subject an observing object to be used for displaying current amount of Valuable.
	 * @param info is not used in this method.
	 */ 
	public void update(Observable subject, Object info) {
		if(subject instanceof Purse ) {
			Purse purse = (Purse) subject;
			if(purse.isFull()) {
				statusLabel.setText("FULL");
			}else if (purse.count()==0) {
				statusLabel.setText("EMPTY");
			}else {
				statusLabel.setText(String.valueOf(purse.count()));
			}
			int progress = purse.count();
			progressBar.setValue(progress);
			this.run();
		}
		if(info != null) System.out.println( info );
	}
}
