package coinpurse;
import java.util.Hashtable;
import java.util.Map;

/**
 * A coupon with a monetary value.
 * Coupon's value cannot be change.
 * @author Pakpon Jetapai
 *
 */
public class Coupon extends AbstractValuable {

    /**map of a color to choose for the coupon*/
    private static Map<String, Double> colorMap;
	static {
		colorMap = new Hashtable<String,Double>();
		colorMap.put("RED", new Double(100) );
		colorMap.put("BLUE", new Double(50) );
		colorMap.put("GREEN", new Double(20) );
	}
	/**color of the coupon*/
	private String color;
	/**
	 * Initialize a new Coupon.
	 * @param color is a String to choose which value this coupon will be.
	 */
	public Coupon(String color){
		super((Double) colorMap.get(color.toUpperCase()).doubleValue());
		this.color = color.toUpperCase();
	}
	/**
	 * Show a String about this coupon's color.
	 * @return color of this coupon.
	 */
	public String toString(){
		if(this.color.equalsIgnoreCase("red")) return "Red coupon";
		if(this.color.equalsIgnoreCase("blue")) return "Blue coupon";
		else return "Green coupon";
	}
	/**
	 * get value of this coupon
	 * @return value of this coupon
	 */
	@Override
	public double getValue() {
		return (double) colorMap.get(this.color.toUpperCase());
	}
}
