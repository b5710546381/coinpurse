package coinpurse;
import java.util.ArrayList;
import java.util.List;

import coinpurse.strategy.*;

/**
 *  A coin purse contains coins.
 *  You can insert coins, withdraw money, check the balance,
 *  and check if the purse is full.
 *  When you withdraw money, the coin purse decides which
 *  coins to remove.
 *  @author Pakpon Jetapai
 */
public class Purse extends java.util.Observable {
	/** Collection of coins in the purse. */
	private List<Valuable> moneyPurse;
	/** Capacity is maximum NUMBER of coins the purse can hold.
	 *  Capacity is set when the purse is created.
	 */
	private int capacity;
	/** ValueComparator object is used for sorting.*/
	private final ValueComparator comparator = new ValueComparator();
	/**Strategy to perform withdraw.*/
	WithdrawStrategy strategy;
	/** 
	 *  Create a purse with a specified capacity.
	 *  @param capacity is maximum number of coins you can put in purse.
	 */
	public Purse( int capacity ) {
		moneyPurse = new ArrayList<Valuable>(capacity);
		this.capacity = capacity;
	}

	/**
	 * Count and return the number of coins in the purse.
	 * This is the number of coins, not their value.
	 * @return the number of coins in the purse
	 */
	public int count() {
		return moneyPurse.size(); 
	}

	/** 
	 *  Get the total value of all items in the purse.
	 *  @return the total value of items in the purse.
	 */
	public double getBalance() {
		double total = 0;
		for (int i = 0;i < moneyPurse.size();i++) {
			total += (double) moneyPurse.get(i).getValue();
		}
		return total;
	}

	/**
	 * Return the capacity of the coin purse.
	 * @return the capacity
	 */
	//TODO write accessor method for capacity. Use Java naming convention.
	public int getCapacity() { 
		return this.capacity; 
	}

	/** 
	 *  Test whether the purse is full.
	 *  The purse is full if number of items in purse equals
	 *  or greater than the purse capacity.
	 *  @return true if purse is full. return false if the purse is not full.
	 */
	public boolean isFull() {
		if (moneyPurse.size() == this.getCapacity()) {
			return true;
		} else {
			return false;
		}
	}

	/** 
	 * Insert a coin into the purse.
	 * The coin is only inserted if the purse has space for it
	 * and the coin has positive value.  No worthless coins!
	 * @param money is a Valuable to insert into purse
	 * @return true if coin inserted, false if can't insert
	 */
	public boolean insert( Valuable money ) {
		// if the purse is already full then can't insert anything.
		if (money.getValue() <= 0 || this.isFull()) {
			return false;
		} else {
			moneyPurse.add(money);
			super.setChanged();
			super.notifyObservers( this );
			return true;
		}
	}

	/**  
	 *  Withdraw the requested amount of money.
	 *  Return an array of Coins withdrawn from purse,
	 *  or return null if cannot withdraw the amount requested.
	 *  @param amount is the amount to withdraw
	 *  @return array of Coin objects for money withdrawn,or null if cannot withdraw requested amount.
	 */
	public Valuable[] withdraw(double amount){
		List<Valuable> temp = strategy.withdraw(amount,moneyPurse);
		double withdrewAmount = 0;
		if(!(temp == null)) {
			for(int i = 0; i < temp.size() ;i++) {
				withdrewAmount += temp.get(i).getValue();
			}
			if (temp.size() >0 && amount-withdrewAmount == 0) {
				for(int i = 0;i<temp.size();i++){
					moneyPurse.remove((Valuable) temp.get(i));
				}
				Valuable[] withdrawCoins = new Valuable[temp.size()];
				for(int i = 0;i<temp.size();i++){
					withdrawCoins[i] = (Valuable) temp.get(i);
				}
				super.setChanged();
				super.notifyObservers();
				return withdrawCoins;
		}
		}
		return null;
	}

	/** 
	 * toString returns a string description of the purse contents.
	 * It can return whatever is a useful description.
	 */
	public String toString() {
		return String.format("%d coins with value %.1f",this.getCapacity(),this.getBalance());
	}
	/**
	 * set a strategy for this purse to use
	 * @param withdrawStrategy a strategy to be used in withdrawing.
	 */
	public void setWithdrawStrategy(WithdrawStrategy withdrawStrategy){
		this.strategy = withdrawStrategy;
	}
}
