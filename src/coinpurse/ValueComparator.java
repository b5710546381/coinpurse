package coinpurse;
import java.util.Comparator;
/**
 * A comparator that compare value between a and b. It also implements Comparator. 
 * @author Pakpon Jetapai
 *
 */
public class ValueComparator implements Comparator<Valuable> {
	
	/**
	 * Compare value between a and b
	 * @param a is a Valuable object that will compare with b
	 * @param b is a Valuable object that will compare with a
	 */
	
	@Override
	public int compare(Valuable a, Valuable b) {
		double aValue = a.getValue();
		double bValue = b.getValue();
		return (int) Math.signum(bValue-aValue);
	}
}

